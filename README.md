<h1>Api ganhatube</h1>

<h3>Get videos</h3>

<p> 
Para recuperar todos os vídeos passe

```python
@GET {base}/video
```

Examplo de retorno:
```javascript
    [
        {
            "_id": "636ba9f24ecc7193baa807e5",
            "repet": false,
            "thumb": "https://img.youtube.com/vi/O3AVvzdQ-JI/0.jpg",
            "title": "test title",
            "url": "https://youtu.be/O3AVvzdQ-JI"
        },
        {
            "_id": "636baa789f52cfe184bd860c",
            "repet": false,
            "thumb": "https://img.youtube.com/vi/O3AVvzdQ/0.jpg",
            "title": "test denovo",
            "url": "https://youtu.be/O3AVvzdQ"
        },
        {
            "_id": "636baa804ecc7193baa807e6",
            "repet": false,
            "thumb": "https://img.youtube.com/vi/O3AVvzdQ/0.jpg",
            "title": "test denovo",
            "url": " https://youtu.be/O3AVvzdQ"
        }
    ]
```
   
</p>

<h3>Post videos</h3>

<p> 
Para inserir um novo vídeo

```python
@POST {base}/video

@BODY 
    {
        "url": "https://youtu.be/O3AVvzdQ-JI", // obrigatório
        "title": "test title" // obrigatório
    }
```


Examplo de retorno:
```javascript
    {
        "msg": "ok"
    }
```
</p>

<h3>Delete videos</h3>

<p> 
Para deletar um vídeo

```python

@DELETE {base}/video

```


Examplo de retorno:
```javascript
    {
        "_id": "636baa804ecc7193baa807e6",
        "repet": false,
        "thumb": "https://img.youtube.com/vi/O3AVvzdQ/0.jpg",
        "title": "test denovo",
        "url": " https://youtu.be/O3AVvzdQ"
    }

```
   
</p>

